var CACHE_NAME = 'pwa-v' + new Date().getTime();
var urlsToCache = [
	'/hors-ligne',
	'/foire-aux-questions',
	'/css/app.css',
	'/webfonts/fa-brands-400.ttf',
	'/webfonts/fa-brands-400.woff2',
	'/webfonts/fa-regular-400.ttf',
	'/webfonts/fa-regular-400.woff2',
	'/webfonts/fa-solid-900.ttf',
	'/webfonts/fa-solid-900.woff2',
	'/js/app.js',
	'/js/manifest.js',
	'/js/vendor.js',
	'/storage/icons/logo.png',
	'/storage/icons/logo.svg',
	'/storage/icons/maskable_icon_x48.png',
	'/storage/icons/maskable_icon_x72.png',
	'/storage/icons/maskable_icon_x96.png',
	'/storage/icons/maskable_icon_x128.png',
	'/storage/icons/maskable_icon_x192.png',
	'/storage/icons/maskable_icon_x384.png',
	'/storage/icons/maskable_icon_x512.png',
	'/storage/icons/maskable_icon_x993.png',
	'/favicon.ico',
	'/manifest.json'
];

// Cache on install
self.addEventListener('install', (event) => {
	self.skipWaiting();
	event.waitUntil(
		caches.open(CACHE_NAME)
			.then(cache => {
				return cache.addAll(urlsToCache);
			})
	);
});

// Clear cache on activate
self.addEventListener('activate', (event) => {
	self.clients.claim();
	event.waitUntil(
		caches.keys().then(cacheNames => {
			return Promise.all(
				cacheNames
					.filter(cacheName => (cacheName.startsWith('pwa-')))
					.filter(cacheName => (cacheName !== CACHE_NAME))
					.map(cacheName => caches.delete(cacheName))
			);
		})
	);
});

// Serve from Cache
self.addEventListener('fetch', (event) => {
	event.respondWith(
		caches.match(event.request)
			.then(response => {
				return response || fetch(event.request);
			})
			.catch(() => {
				return caches.match('hors-ligne');
			})
	);
});
