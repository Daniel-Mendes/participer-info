/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue').default;

import { createApp } from 'vue';
import dayjs from 'dayjs';
import duration from 'dayjs/plugin/duration';
import relativeTime from 'dayjs/plugin/relativeTime';
import fr from 'dayjs/locale/fr';

dayjs.extend(relativeTime, duration);
dayjs.locale(fr);

window.dayjs = dayjs;

import algoliasearch from 'algoliasearch/lite';
window.algoliasearch = algoliasearch;

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = createApp({});

app.component('ArticleCreateForm', require('./components/articles/CreateFormComponent.vue').default);
app.component('ArticleShow', require('./components/articles/ShowComponent.vue').default);
app.component('CategoriesShow', require('./components/categories/ShowComponent.vue').default);
app.component('ArticlesIndex', require('./components/articles/IndexComponent.vue').default);

app.component('AdminLineChart', require('./components/admin/LineChart.vue').default);
app.component('AdminDoughnutChart', require('./components/admin/DoughnutChart.vue').default);
app.component('AdminSearch', require('./components/admin/SearchComponent.vue').default);

app.mount('#app');
