@extends('layouts.app')

@section('title', 'Partager un lien')

@section('content')
<div class="container">
    <div class="row justify-content-center mb-5">
        <div class="col-lg-8">
            <article-create-form :categories="{{ json_encode($categories) }}" />
        </div>
    </div>
</div>
@endsection
