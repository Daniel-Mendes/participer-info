@extends('layouts.app')

@section('title', 'Tous les articles')

@section('content')
@include('layouts.navigation')

<section class="container">
	<div class="row">
		<div class="col-12 col-lg-10 mx-auto">
			<h1 class="mb-4 display-5 fw-bold text-dark-green title-underline-green">Tous les articles</h1>
			<articles-index :badge="true" />
		</div>
	</div>
</section>
@endsection
