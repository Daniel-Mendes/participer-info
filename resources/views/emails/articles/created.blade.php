@component('mail::message')
# Un nouvel article a été partagé le {{ $article->created_at->format('d.m.Y') }}

@component('mail::table')
|           Catégorie       |         Titre         |           Autheur       |
| ------------------------- | --------------------- | --------------------------- |
| {{ $article->category->name  }} | {{ $article->title }} | {{ $article->author }} |
@endcomponent

@component('mail::button', ['url' => route('admin.articles.show', $article->id)])
Voir l'article
@endcomponent

Merci,<br>
{{ config('app.name') }}
@endcomponent
