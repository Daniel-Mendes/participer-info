@extends('layouts.app')

@section('title', 'Hors-ligne')

@section('content')
<div class="container">
    <div class="row my-5 justify-content-center text-center">
        <div class="col-12 col-lg-8">
            <i class="fa-regular fa-face-sad-tear fa-4x mb-4"></i>
            <h1 class="mb-3">{{ __('offline.title') }}</h1>
            <h3 class="mb-4">{{ __('offline.text') }}</h3>
            <a href="" class="btn bg-dark-green btn-lg text-white">Réessayer</a>
        </div>
    </div>
</div>
@endsection
