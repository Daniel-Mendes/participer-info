@extends('layouts.admin.app')

@section('content')
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1>Détails de l'article</h1>
    <div class="d-flex">
        <a href="{{ route("admin.articles.edit", $article->id) }}" class="btn btn-warning text-white me-2">Modifier l'article</a>
        <form method="POST" action="{{ route('admin.articles.destroy', $article->id) }}">
            @csrf
            @method('DELETE')

            <div class="form-group">
                <input type="submit" class="btn btn-danger text-white" value="Supprimer l'article">
            </div>
        </form>
    </div>
</div>

<div class="my-4">
    <div class="col">
        <div class="card">
            <div class="card-body py-0">
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">
                        <div class="row my-2">
                            <span class="col-2 fw-bold">ID</span>
                            <span class="col-10">{{ $article->id }}</span>
                        </div>
                    </li>
                    <li class="list-group-item">
                        <div class="row my-2">
                            <span class="col-2 fw-bold">URL</span>
                            <span class="col-10">
                                <a href="{{ $article->url }}">{{ $article->url }}</a>
                            </span>
                        </div>
                    </li>
                    <li class="list-group-item">
                        <div class="row my-2">
                            <span class="col-2 fw-bold">Titre</span>
                            <span class="col-10">{{ $article->title }}</span>
                        </div>
                    </li>
                    <li class="list-group-item">
                        <div class="row my-2">
                            <span class="col-2 fw-bold">Slug</span>
                            <span class="col-10">{{ $article->slug }}</span>
                        </div>
                    </li>
                    <li class="list-group-item">
                        <div class="row my-2">
                            <span class="col-2 fw-bold">Description</span>
                            <span class="col-10">{{ $article->description }}</span>
                        </div>
                    </li>
                    <li class="list-group-item">
                        <div class="row my-2">
                            <span class="col-2 fw-bold">Auteur</span>
                            <span class="col-10">{{ $article->author }}</span>
                        </div>
                    </li>
                    <li class="list-group-item">
                        <div class="row my-2">
                            <span class="col-2 fw-bold">Image</span>
                            <span class="col-10">
                                <img src="{{ $article->image }}" class="img-fluid" style="max-height: 400px;"/>
                            </span>
                        </div>
                    </li>
                    <li class="list-group-item">
                        <div class="row my-2">
                            <div class="col-2 fw-bold">Statut</div>
                            <div class="col-10">
                                @switch($article->status)
                                    @case(\App\Enums\ArticleStatus::PENDING)
                                    <span class="badge bg-warning fs-6">En attente</span>
                                    @break

                                    @case(\App\Enums\ArticleStatus::PUBLISHED)
                                    <span class="badge bg-success fs-6">Publié</span>
                                    @break

                                    @case(\App\Enums\ArticleStatus::REJECTED)
                                    <span class="badge bg-danger fs-6">Rejeté</span>
                                @endswitch
                            </div>
                        </div>
                    </li>
                    <li class="list-group-item">
                        <div class="row my-2">
                            <span class="col-2 fw-bold">Catégorie</span>
                            <span class="col-10">{{ $article->category_id ? $article->category->name : "" }}</span>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
@endsection
