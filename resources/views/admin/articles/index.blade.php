@extends('layouts.admin.app')

@section('content')
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1>Articles</h1>
</div>

<div class="my-4 table-responsive">
    <table class="table table-striped table-hover align-middle">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Titre</th>
                <th scope="col">Statut</th>
                <th scope="col">Actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($articles as $article)
                <tr>
                    <th scope="row">{{ $article->id }}</th>
                    <td>{{ Str::limit($article->title, 120) }}</td>
                    @switch($article->status)
                        @case(\App\Enums\ArticleStatus::PENDING)
                        <td><span class="badge bg-warning">En attente</span></td>
                        @break

                        @case(\App\Enums\ArticleStatus::PUBLISHED)
                        <td><span class="badge bg-success">Publié</span></td>
                        @break

                        @case(\App\Enums\ArticleStatus::REJECTED)
                        <td><span class="badge bg-danger">Rejeté</span></td>
                    @endswitch
                    <td>
                        <div class="btn-group" role="group" aria-label="Actions">
                            <a href="{{ route('admin.articles.show', $article->id) }}" class="btn btn-primary"><i class="fas fa-eye text-white"></i></a>
                            <a href="{{ route('admin.articles.edit', $article->id) }}" class="btn btn-warning"><i class="fas fa-pen text-white"></i></a>
                            <form method="POST" action="{{ route('admin.articles.destroy', $article->id) }}">
                                @csrf
                                @method('DELETE')

                                <button type="submit" class="btn btn-danger rounded-0 rounded-end">
                                    <i class="fas fa-trash-alt text-white"></i>
                                </button>
                            </form>
                        </div>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>

<div class="d-flex justify-content-center">
    {{ $articles->links() }}
</div>
@endsection
