@extends('layouts.admin.app')

@section('content')
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1>Modifier l'article</h1>
    <div>
        <a href="{{ route("admin.articles.show", $article->id) }}" class="btn btn-primary text-white me-2">Voir l'article</a>
        <a href="{{ route('admin.articles.destroy', $article->id) }}" class="btn btn-danger text-white">Supprimer l'article</a>
    </div>
</div>

<div class="my-4">
    <div class="col">
        <div class="card">
            <div class="card-body p-3">
                <form method="POST" action="{{ route('admin.articles.update', $article->id) }}">
                    @csrf
                    @method('PUT')

                    <div class="mb-3">
                        <label for="url" class="form-label">URL</label>
                        <input id="url" type="text" class="form-control @error('url') is-invalid @enderror" name="url" placeholder="{{ __('URL') }}" value="{{ old('url', $article->url) }}" required/>

                        @error('url')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="mb-3">
                        <label for="title" class="form-label">Titre</label>
                        <input id="title" type="text" class="form-control @error('title') is-invalid @enderror" name="title" placeholder="{{ __('Titre') }}" value="{{ old('title', $article->title) }}" required/>

                        @error('title')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="mb-3">
                        <label for="slug" class="form-label">Slug</label>
                        <input id="slug" type="text" class="form-control @error('slug') is-invalid @enderror" name="slug" placeholder="{{ __('Slug') }}" value="{{ old('slug', $article->slug) }}" required/>

                        @error('slug')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="mb-3">
                        <label for="description" class="form-label">Description</label>
                        <textarea name="description" id="description" class="form-control @error('description') is-invalid @enderror" rows="2" placeholder="Description">{{ old('description', $article->description) }}</textarea>

                        @error('description')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="mb-3">
                        <label for="author" class="form-label">Auteur</label>
                        <input id="author" type="text" class="form-control @error('author') is-invalid @enderror" name="author" placeholder="Auteur" value="{{ old('author', $article->author) }}" required/>

                        @error('author')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="mb-3">
                        <label for="image" class="form-label">Image</label>
                        <input id="image" name="image" type="file" class="form-control @error('image') is-invalid @enderror" accept="image/*"/>

                        <figure class="mt-3">
                            <figcaption class="text-muted">Image actuelle</figcaption>
                            <img src="{{ $article->image }}" class="rounded" style="max-height: 300px;"/>
                        </figure>

                        @error('image')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="mb-3">
                        <label for="status" class="form-label">Statut</label>
                        <select class="form-select" id="status" name="status" aria-label="Sélectionnez le statut de l'article">
                            @foreach (\App\Enums\ArticleStatus::cases()	 as $status)
                                <option value="{{ $status->value }}" {{ old('status', $article->status) == $status ? "selected" : "" }}>
                                    @switch($status)
                                        @case(\App\Enums\ArticleStatus::PENDING)
                                            En attente
                                            @break

                                        @case(\App\Enums\ArticleStatus::PUBLISHED)
                                            Publié
                                            @break

                                        @case(\App\Enums\ArticleStatus::REJECTED)
                                            Rejeté
                                            @break
                                    @endswitch
                                </option>
                            @endforeach
                        </select>

                        @error('status')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="mb-3">
                        <label for="category" class="form-label">Catégorie</label>
                        <select class="form-select" id="category" name="category" aria-label="Sélectionnez la catégorie de l'article">
                            <option value="" selected>Aucune</option>
                            @foreach ($categories as $category)
                                <option value="{{ $category->id }}" {{ old('category', $article->category_id) == $category->id ? "selected" : "" }}>{{ $category->name }}</option>
                            @endforeach
                        </select>

                        @error('category')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <button type="submit" class="btn btn-warning btn-md text-white">Modifier l'article</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
