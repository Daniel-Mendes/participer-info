@extends('layouts.admin.app')

@section('content')
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1>{{ __("admin.index.status.${status}") }}</h1>
</div>

<div class="my-4 table-responsive">
    <table class="table table-striped table-hover align-middle">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Catégorie</th>
                <th scope="col">Titre</th>
                <th scope="col">Flux RSS</th>
                <th scope="col">Date de création</th>
                <th scope="col">Actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($articles as $article)
                <tr>
                    <th scope="row">{{ $article->id }}</th>
                    <th>{{ $article->category ? $article->category->name : '' }}</th>
                    <td>{{ Str::limit($article->title, 120) }}</td>
                    <td>{{ $article->feed ? $article->feed->name : '' }}</td>
                    <td>{{ $article->created_at->diffForHumans() }}</td>
                    <td>
                        <div class="btn-group" role="group" aria-label="Actions">
                            <a href="{{ route('admin.articles.show', $article->id) }}" class="btn btn-primary"><i class="fas fa-eye text-white"></i></a>
                            <a href="{{ route('admin.articles.edit', $article->id) }}" class="btn btn-warning"><i class="fas fa-pen text-white"></i></a>
                            <a href="{{ route('admin.articles.destroy', $article->id) }}" class="btn btn-danger"><i class="fas fa-trash-alt text-white"></i></a>
                        </div>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>

<div class="d-flex justify-content-center">
    {{ $articles->links() }}
</div>
@endsection
