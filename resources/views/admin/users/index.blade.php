@extends('layouts.admin.app')

@section('content')
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1>Utilisateurs</h1>
</div>

<div class="my-4">
    <div class="table-responsive">
        <table class="table table-striped table-hover align-middle">
            <thead>
                <tr>
                <th scope="col">#</th>
                <th scope="col">Nom</th>
                <th scope="col">Email</th>
                <th scope="col">Est-il admin ?</th>
                <th scope="col">Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($users as $user)
                    <tr>
                        <th scope="row">{{ $user->id }}</th>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->is_admin ? 'Oui' : 'Non' }}</td>
                        <td>
                            <div class="btn-group" role="group" aria-label="Actions">
                                <a href="{{ route('admin.users.show', $user->id) }}" class="btn btn-primary" type="button"><i class="fas fa-eye text-white"></i></a>
                                <a href="{{ route('admin.users.edit', $user->id) }}" class="btn btn-warning" type="button"><i class="fas fa-pen text-white"></i></a>
                                <a href="{{ route('admin.users.destroy', $user->id) }}" class="btn btn-danger" type="button"><i class="fas fa-trash-alt text-white"></i></a>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

<div class="d-flex justify-content-center">
    {{ $users->links() }}
</div>
@endsection
