@extends('layouts.admin.app')

@section('content')
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1>{{ __('admin/feeds.index.title') }}</h1>
    <a href="{{ route('admin.feeds.create') }}" class="btn btn-success text-white">{{ __('admin/feeds.index.create') }}</a>
</div>

<div class="my-4 table-responsive">
    <table class="table table-striped table-hover align-middle">
        <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">{{ __('Name') }}</th>
              <th scope="col">Dernière synchronisation</th>
              <th scope="col">{{ __('Actions') }}</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($feeds as $feed)
                <tr>
                    <th scope="row">{{ $feed->id }}</th>
                    <td>{{ Str::limit($feed->name, 120) }}</td>
                    <td>{{ \Carbon\Carbon::parse($feed->last_sync)->format('d.m.Y à H:i') }}</td>
                    <td>
                        <div class="btn-group" role="group" aria-label="{{ __('Actions') }}">
                            <a href="{{ route('admin.feeds.show', $feed->id) }}" class="btn btn-primary"><i class="fas fa-eye text-white"></i></a>
                            <a href="{{ route('admin.feeds.edit', $feed->id) }}" class="btn btn-warning"><i class="fas fa-pen text-white"></i></a>
                            <form method="POST" action="{{ route('admin.feeds.destroy', $feed->id) }}">
                                @csrf
                                @method('DELETE')

                                <button type="submit" class="btn btn-danger rounded-0 rounded-end">
                                    <i class="fas fa-trash-alt text-white"></i>
                                </button>
                            </form>
                        </div>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>

<div class="d-flex justify-content-center">
    {{ $feeds->links() }}
</div>
@endsection
