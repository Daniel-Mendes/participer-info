@extends('layouts.admin.app')

@section('content')
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1>Modifier le flux RSS</h1>
    <div>
        <a href="{{ route("admin.feeds.show", $feed->id) }}" class="btn btn-primary text-white me-2">Voir le flux RSS</a>
        <a href="{{ route('admin.feeds.destroy', $feed->id) }}" class="btn btn-danger text-white">Supprimer le flux RSS</a>
    </div>
</div>

<div class="my-4">
    <div class="col">
        <div class="card">
            <div class="card-body p-3">
                <form method="POST" action="{{ route('admin.feeds.update', $feed->id) }}">
                    @csrf
                    @method('PUT')

                    <div class="mb-3">
                        <label for="name" class="form-label">Nom</label>
                        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" placeholder="{{ __('Nom') }}" value="{{ old('name', $feed->name) }}" required/>

                        @error('title')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="mb-3">
                        <label for="slug" class="form-label">Slug</label>
                        <input id="slug" type="text" class="form-control @error('slug') is-invalid @enderror" name="slug" placeholder="{{ __('Slug') }}" value="{{ old('slug', $feed->slug) }}" required/>

                        @error('slug')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="mb-3">
                        <label for="url" class="form-label">URL</label>
                        <input id="url" type="text" class="form-control @error('url') is-invalid @enderror" name="url" placeholder="{{ __('URL') }}" value="{{ old('url', $feed->url) }}" required/>

                        @error('url')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <button type="submit" class="btn btn-warning btn-md text-white">Modifier le flux RSS</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
