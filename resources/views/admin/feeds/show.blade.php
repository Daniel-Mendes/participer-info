@extends('layouts.admin.app')

@section('content')
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1>Détails du flux RSS</h1>
    <div class="d-flex">
        <a href="{{ route('admin.feeds.edit', $feed->id) }}" class="btn btn-warning text-white me-2">Modifier le flux RSS</a>
        <form method="POST" action="{{ route('admin.feeds.destroy', $feed->id) }}">
            @csrf
            @method('DELETE')

            <div class="form-group">
                <input type="submit" class="btn btn-danger text-white" value="Supprimer le flux RSS">
            </div>
        </form>
    </div>
</div>

<div class="row my-4">
    <div class="col">
        <div class="card">
            <div class="card-body py-0">
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">
                        <div class="row my-2">
                            <span class="col-2 fw-bold">ID</span>
                            <span class="col-10">{{ $feed->id }}</span>
                        </div>
                    </li>
                    <li class="list-group-item">
                        <div class="row my-2">
                            <span class="col-2 fw-bold">Nom</span>
                            <span class="col-10">{{ $feed->name }}</span>
                        </div>
                    </li>
                    <li class="list-group-item">
                        <div class="row my-2">
                            <span class="col-2 fw-bold">Slug</span>
                            <span class="col-10">{{ $feed->slug }}</span>
                        </div>
                    </li>
                    <li class="list-group-item">
                        <div class="row my-2">
                            <span class="col-2 fw-bold">URL</span>
                            <span class="col-10">
                                <a href="{{ $feed->url }}">{{ $feed->url }}</a>
                            </span>
                        </div>
                    </li>
                    <li class="list-group-item">
                        <div class="row my-2">
                            <span class="col-2 fw-bold">Dernière synchronisation</span>
                            <span class="col-10">{{ \Carbon\Carbon::parse($feed->last_sync)->format('d.m.Y à H:i') }}</span>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1>Articles liés au flux RSS</h1>
</div>

<div class="my-4 table-responsive">
    <div clas="col table-responsive">
        <table class="table table-striped table-hover align-middle">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Titre</th>
                <th scope="col">Statut</th>
                <th scope="col">Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($articles as $article)
                <tr>
                    <th scope="row">{{ $article->id }}</th>
                    <td>{{ Str::limit($article->title, 120) }}</td>
                    @switch($article->status->name)
                        @case('PENDING')
                        <td><span class="badge bg-warning">En attente</span></td>
                        @break

                        @case('PUBLISHED')
                        <td><span class="badge bg-success">Publié</span></td>
                        @break

                        @case('REJECTED')
                        <td><span class="badge bg-danger">Rejeté</span></td>
						@break
                    @endswitch
                    <td>
                        <div class="btn-group" role="group" aria-label="Actions">
                            <a href="{{ route('admin.articles.show', $article->id) }}" class="btn btn-primary"><i class="fas fa-eye text-white"></i></a>
                            <a href="{{ route('admin.articles.edit', $article->id) }}" class="btn btn-warning"><i class="fas fa-pen text-white"></i></a>
                            <a href="{{ route('admin.articles.destroy', $article->id) }}" class="btn btn-danger"><i class="fas fa-trash-alt text-white"></i></a>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>

<div class="row">
    <div class="d-flex justify-content-center">
        {{ $articles->links() }}
    </div>
</div>


@endsection
