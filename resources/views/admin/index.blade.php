@extends('layouts.admin.app')

@section('content')
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1>{{ __('admin.index.title') }}</h1>
</div>

<div class="row">
    <div class="col-12 col-md-4 mb-4">
        <div class="card border-0 border-start border-warning border-4 shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs fw-bold text-warning text-uppercase mb-1">
                            {{ __('admin.index.status.pending') }}
                        </div>
                        <div class="h5 mb-0 fw-bold text-dark">{{ $articlesCountGroupByStatus[\App\Enums\ArticleStatus::PENDING->value]['total'] }}</div>
                    </div>
                    <div class="col-auto">
                        <i class="fa-regular fa-newspaper fa-2x text-muted"></i>
                    </div>
                    <a href="{{ route('admin.articles.status', ['status' => "pending"]) }}" class="stretched-link"></a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-12 col-md-4 mb-4">
        <div class="card border-0 border-start border-success border-4 shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs fw-bold text-success text-uppercase mb-1">
                            {{ __('admin.index.status.published') }}
                        </div>
                        <div class="h5 mb-0 fw-bold text-dark">{{ $articlesCountGroupByStatus[\App\Enums\ArticleStatus::PUBLISHED->value]['total'] }}</div>
                    </div>
                    <div class="col-auto">
                        <i class="fa-regular fa-newspaper fa-2x text-muted"></i>
                    </div>
                    <a href="{{ route('admin.articles.status', ['status' => "published"]) }}" class="stretched-link"></a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-12 col-md-4 mb-4">
        <div class="card border-0 border-start border-danger border-4 shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs fw-bold text-danger text-uppercase mb-1">
                            {{ __('admin.index.status.rejected') }}
                        </div>
                        <div class="h5 mb-0 fw-bold text-dark">{{ $articlesCountGroupByStatus[\App\Enums\ArticleStatus::REJECTED->value]['total'] }}</div>
                    </div>
                    <div class="col-auto">
                        <i class="fa-regular fa-newspaper fa-2x text-muted"></i>
                    </div>
                    <a href="{{ route('admin.articles.status', ['status' => "rejected"]) }}" class="stretched-link"></a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <!-- Area Chart -->
    <div class="col-xl-8 col-lg-7">
        <div class="card shadow mb-4">
            <!-- Card Header - Dropdown -->
            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 fw-bold text-primary">{{ __('admin.index.charts.items') }}</h6>
            </div>
            <!-- Card Body -->
            <div class="card-body">
                <div class="chart-area">
                    <admin-line-chart :data="{{ json_encode($articlesChartjsData) }}" />
				</div>
            </div>
        </div>
    </div>

    <!-- Pie Chart -->
    <div class="col-xl-4 col-lg-5">
        <div class="card shadow mb-4">
            <!-- Card Header - Dropdown -->
            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 fw-bold text-primary">{{ __('admin.index.charts.categories') }}</h6>
            </div>
            <!-- Card Body -->
            <div class="card-body">
                <div class="chart-pie">
                    <admin-doughnut-chart :data="{{ json_encode($categoriesChartjsData) }}" />
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
