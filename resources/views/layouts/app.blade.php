<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title') - {{ config('app.name', 'Laravel') }}</title>

    <!-- Description -->
    <meta name="description" content="@yield('description', "participer.info est une plateforme qui a pour but de partager des articles, vidéos, news, etc., afin de mettre en avant les visions alternatives des citoyens.")">

    <!-- OpenGraph - image -->
	<meta property="og:image" content="{{ asset('storage/participer-info.png') }}">

	<!-- Favicon -->
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">

	<!-- JSON Feed -->
	@include('feed::links')

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">

    <!-- Manifest -->
    <link rel="manifest" href="/manifest.json">
</head>
<body>
    <header>
        @include('layouts.nav')
    </header>

    <main>
        <div id="app">
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif

            @yield('content')
        </div>
    </main>

    @include('layouts.footer')

    <!-- Scripts -->
    <script src="{{ asset('js/manifest.js') }}"></script>
    <script src="{{ asset('js/vendor.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>
	@production
		<script async defer src="https://beampipe.io/js/tracker.js" data-beampipe-domain="participer.info"></script>
	@endproduction
    <script type="text/javascript">
        // Initialize the service worker
        window.addEventListener('load', () => {
            if ('serviceWorker' in navigator) {
                console.log('Service worker is supported');
                navigator.serviceWorker.register('/sw.js', { scope: '/' }).then(registration => {
                    console.log('Service worker is registered', registration);
                }).catch(error => {
                    console.log('Service worker registration failed', error);
                });
            }
        });
    </script>
</body>
</html>
