<!-- Footer -->
<footer class="footer text-center text-lg-start bg-white">
    <!-- Section: Links  -->
    <section>
      <div class="container text-center text-md-start mt-5">
        <!-- Grid row -->
        <div class="row mt-3">
          <!-- Grid column -->
          <div class="col-md-4 col-xl-3 mx-auto mb-4">
            <!-- Content -->
            <h2 class="text-uppercase h6 fw-bold mb-4 text-dark-green">
              {{ config('app.name', 'Laravel') }}
            </h2>
            <p class="text-muted">
              <span class="fw-bold">participer.info</span> est une plateforme qui a pour but de partager des articles, vidéos, news, etc., afin de mettre en avant les visions alternatives des citoyens.
            </p>
          </div>
          <!-- Grid column -->

          <!-- Grid column -->
          <div class="col-md-4 col-lg-2 col-xl-2 mx-auto text-center mb-4">
            <!-- Links -->
            <h2 class="text-uppercase h6 fw-bold mb-4 text-dark-green">
                Liens utiles
            </h2>
            <ul class="list-unstyled">
                <li>
                    <a href="{{ route('articles.create') }}">Partager un lien</a>
                </li>
                <li>
                    <a href="{{ route('faq') }}">FAQ</a>
                </li>
                <li>
                    <a href="/feed">Flux RSS</a>
                </li>
            </ul>
          </div>
          <!-- Grid column -->

          <!-- Grid column -->
          <div class="col-md-4 col-lg-3 col-xl-3 mx-auto text-center mb-md-0 mb-4">
            <!-- Links -->
            <h2 class="text-uppercase h6 fw-bold mb-4 text-dark-green">
              Contact
            </h2>
            <ul class="list-unstyled">
                <li>
                    <a href="mailto:contactez@participer.info" target="_blank" rel="noreferrer noopener">
                        contactez@participer.info
                    </a>
                </li>
            </ul>
          </div>
          <!-- Grid column -->
        </div>
        <!-- Grid row -->
      </div>
    </section>
    <!-- Section: Links  -->

    <!-- Copyright -->
    <div class="text-center p-3 text-white bg-dark-green">
      © {{ \Carbon\Carbon::now()->year }} Créé par
      <a class="text-reset" href="https://daniel-mendes.ch/" target="_blank" rel="noreferrer noopener">Daniel Mendes</a>
    </div>
    <!-- Copyright -->
  </footer>
  <!-- Footer -->
