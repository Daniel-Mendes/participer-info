<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

	<meta name="robots" content="noindex">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Tableau de bord - {{ config('app.name', 'Laravel') }}</title>

    <!-- Description -->
    <meta name="description" content="participer.info est une plateforme qui a pour but de partager des articles, vidéos, news, etc., afin de mettre en avant les visions alternatives des citoyens.">

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">

    <!-- Manifest -->
    <link rel="manifest" href="/manifest.json">
</head>
<body>
    @include('layouts.admin.header')

    <div class="container-fluid">
        <div class="row">
            @include('layouts.admin.nav')

            <div id="app">
                <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
                    <admin-search></admin-search>

                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    @yield('content')
                </main>
            </div>
        </div>
    </div>
    <!-- Scripts -->
    <script src="{{ asset('js/manifest.js') }}"></script>
    <script src="{{ asset('js/vendor.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script type="text/javascript">
        // Initialize the service worker
        window.addEventListener('load', () => {
            if ('serviceWorker' in navigator) {
                console.log('Service worker is supported');
                navigator.serviceWorker.register('/sw.js', { scope: '/' }).then(registration => {
                    console.log('Service worker is registered', registration);
                }).catch(error => {
                    console.log('Service worker registration failed', error);
                });
            }
        });
    </script>
</body>
</html>
