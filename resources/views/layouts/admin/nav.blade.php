<nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">
    <div class="position-sticky pt-3">
        <ul class="nav flex-column">
            <li class="nav-item">
                <a class="nav-link {{ Route::is('admin.index') ? 'active' : '' }}" aria-current="page" href="{{ route('admin.index') }}">
                    <i class="fa-solid fa-chart-line"></i>
                    {{ __('admin.layout.nav.dashboard') }}
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ Route::is('admin.articles.*') ? 'active' : '' }}" href="{{ route('admin.articles.index') }}">
                    <i class="fa-regular fa-newspaper"></i>
                    {{ __('admin.layout.nav.items') }}
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ Route::is('admin.feeds.*') ? 'active' : '' }}" href="{{ route('admin.feeds.index') }}">
                    <i class="fa-solid fa-rss"></i>
                    {{ __('admin.layout.nav.feeds') }}
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link  {{ Route::is('admin.users.*') ? 'active' : '' }}" href="{{ route('admin.users.index') }}">
                    <i class="fa-solid fa-users"></i>
                    {{ __('admin.layout.nav.users') }}
                </a>
            </li>
            <li class="nav-item d-md-none">
                <a class="nav-link px-3" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                    <i class="fa-solid fa-right-from-bracket"></i>
                    {{ __('Logout') }}
                </a>
            </li>
        </ul>
    </div>
</nav>
