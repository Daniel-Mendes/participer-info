<nav class="navbar navbar-dark bg-dark-green py-3 py-lg-4">
    <div class="container">
        <div class="row w-100 g-0 align-items-center justify-content-center">
            <div class="col-3 col-lg-2" id="nav">
                <button id="nav-toggler" class="navbar-toggler" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvas-navbar" aria-controls="offcanvas-navbar" aria-expanded="false" aria-label="Menu déroulant">
                    <i class="fa-solid fa-bars-staggered fa-2x text-white"></i>
                </button>
                <div class="offcanvas offcanvas-start bg-light" id="offcanvas-navbar">
                    <div class="offcanvas-header">
                        <a class="fs-2 fw-bold text-decoration-none text-dark-green mb-0" href="#">{{ config('app.name', 'Laravel') }}</a>
                        <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Fermer"></button>
                    </div>
                    <div class="offcanvas-body">
                        <ul class="navbar-nav justify-content-end flex-grow-1 pe-3">
                            <ul class="list-unstyled mb-4">
                              <li class="nav-item">
									<p class="text-muted fs-5">
										<span class="fw-bold">participer.info</span> est une plateforme qui a pour but de partager des articles, vidéos, news, etc., afin de mettre en avant les visions alternatives des citoyens.
									</p>
                            	</li>
                            </ul>
                            <ul class="list-unstyled mb-4">
                                <h2 class="fs-3 fw-bold mb-2 text-dark-green">Liens utiles</h2>
                                <li class="nav-item">
                                    <a class="nav-link text-dark-green" href="{{ route('articles.create') }}">Partager un lien</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link text-dark-green" href="{{ route('faq') }}">FAQ</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link text-dark-green" href="/feed">Flux RSS</a>
                                </li>
                            </ul>
                            <ul class="list-unstyled">
                                <h2 class="fs-3 fw-bold mb-2 text-dark-green">Contact</h2>
                                <li class="nav-item">
                                    <a class="nav-link text-dark-green" href="mailto:contactez@participer.info" target="_blank" rel="noreferrer noopener">
                                        contactez@participer.info
                                    </a>
                                </li>
                            </ul>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-6 col-lg-6 text-start text-lg-center">
                <a href="/" class="d-inline-block">
                    <object data="{{ asset('storage/icons/logo.svg') }}" type="image/svg+xml" class="nav-logo">
                        <img src="{{ asset('storage/icons/logo.png') }}" alt="Le logo de participer.info" class="nav-logo" />
                    </object>
                </a>
            </div>
            <div class="col-3 col-lg-2 text-end">
                <a href="{{ route('articles.create') }}" class="btn btn-articles-create d-none d-lg-inline-block">Partager un lien</a>
                <a href="{{ route('articles.create') }}" class="d-lg-none"><i class="fa-solid fa-circle-plus fa-3x text-white" aria-label="Partager un lien"></i></a>
            </div>
        </div>
    </div>
</nav>
