<section class="sticky-top bg-white shadow-sm py-2 mb-5">
	<div class="container">
		<div class="row">
			<ul class="categories-container col-12 col-lg-10 mx-auto">
				@foreach ($categories as $category)
					<li class="@if  (isset($categoryActive) && $category->slug === $categoryActive->slug) active @endif">
						<a href="{{ route('categories.show', ['category' => $category->slug]) }}" class="h5">{{ $category->name }}</a>
					</li>
				@endforeach
			</ul>
		</div>
	</div>
</section>
