@extends('layouts.app')

@section('title', $categoryActive->name)
@section('description', "Tous les articles concernant " . (preg_match('/^[aeiou]/i', $categoryActive->slug) ? "l'" : "la ") . strtolower($categoryActive->name) . " sur " . config('app.name'))

@section('content')

@include('layouts.navigation')

<section class="container">
    <div class="row">
        <div class="col-12 col-lg-10 mx-auto">
			<h1 class="mb-4 display-5 fw-bold text-dark-green title-underline-green" id="categorie-title">{{ $categoryActive->name }}</h1>
            <categories-show :badge="false" category="{{ $categoryActive->slug }}" />
        </div>
    </div>
</section>
@endsection
