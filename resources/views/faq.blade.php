@extends('layouts.app')

@section('title', 'FAQ')

@section('content')
<div class="container faq">
    <section class="row mb-3 justify-content-center">
        <div class="col-12 col-lg-8">
            <div class="my-4 text-center">
                <h1 class="fw-bold text-dark-green title-underline-green title-underline-center">Foire aux questions</h1>
            </div>

            <ul class="d-grid gap-4 mb-5 list-unstyled">
                <li>
                    <div class="card border-0 border-start border-5 border-green py-3">
                        <div class="card-body">
                            <h3 class="card-title pb-2">Pourquoi il n'y a pas de section commentaire&nbsp;?</h3>
                            <p class="card-text">
                                Nous sommes seulement une plateforme d'agrégation de contenu, nous voulons que les commentaires restent uniquement sur les articles pour que la communauté puisse interagir ensemble.
                            </p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="card border-0 border-start border-5 border-green py-3">
                        <div class="card-body">
                            <h3 class="card-title pb-2">Comment le contenu est ajouté&nbsp;?</h3>
                            <div class="card-text">
                                Il y a deux méthodes qui sont utilisées pour ajouter un nouveau contenu&nbsp;:
                                <ul>
                                    <li>
                                        En cliquant sur le bouton "Partager un lien" dans la barre de navigation.
                                    </li>
                                    <li>
                                        Via un flux RSS ajouté par nous qui analyse un site web, une chaîne YouTube, un blog, etc.
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="card border-0 border-start border-5 border-green py-3">
                        <div class="card-body">
                            <h3 class="card-title pb-2">Comment est financé le projet&nbsp;?</h3>
                            <div class="card-text">
                                <p>Le projet est actuellement géré par une seule personne bénévole.</p>
                                <ul>
                                    <li>Il n'y a pas de publicité.</li>
                                    <li>Il n'y a évidemment pas d'offre payante, car le contenu ne nous appartient pas.</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="card border-0 border-start border-5 border-green py-3">
                        <div class="card-body">
                            <h3 class="card-title pb-2">Y a-t-il seulement du contenu en français&nbsp;?</h3>
                            <p class="card-text">
                                La plateforme est destinée à un public français, cependant, vous pouvez partager du contenu dans une autre langue s'il peut être traduit avec des outils ou bien que la vidéo comporte des sous-titres en français.
                            </p>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </section>
</div>
@endsection
