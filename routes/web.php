<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get(
	'/',
	[App\Http\Controllers\ArticleController::class, 'index']
)->name('home');

Auth::routes([
	'register' => false,
]);

Route::feeds();

Route::post(
	'articles',
	[App\Http\Controllers\ArticleController::class, 'store']
);

Route::get(
	'articles/creation',
	[App\Http\Controllers\ArticleController::class, 'create']
)->name('articles.create');

Route::get(
	'categories/{category}/articles',
	[App\Http\Controllers\CategoryController::class, 'show']
)->name('categories.show');

Route::get('foire-aux-questions', function () {
	return view('faq');
})->name('faq');

Route::get('hors-ligne', function () {
	return view('offline');
})->name('offline');

Route::post(
	'/articles/create/fetch',
	[App\Http\Controllers\ArticleController::class, 'fetch']
);

Route::middleware(['auth.admin'])->name('admin.')->prefix('admin')->group(function () {
	Route::get(
		'/',
		[App\Http\Controllers\Admin\AdminController::class, 'index']
	)->name('index');

	Route::get(
		'articles/status/{status}',
		[App\Http\Controllers\Admin\ArticleController::class, 'status']
	)->name('articles.status');

	Route::resource('articles', App\Http\Controllers\Admin\ArticleController::class);

	Route::resource('feeds', App\Http\Controllers\Admin\FeedController::class);

	Route::resource('users', App\Http\Controllers\Admin\UserController::class);
});
