<?php

/*
|--------------------------------------------------------------------------
| Admin Language Lines
|--------------------------------------------------------------------------
*/

return [
	'layout'       => [
		'nav' => [
			'dashboard' => 'Tableau de bord',
			'users'     => 'Utilisateurs',
			'items'     => 'Articles',
			'feeds'     => 'Flux RSS',
			'users'     => 'Utilisateurs',
		],
	],
	'index'         => [
		'title'  => 'Tableau de bord',
		'status' => [
			'published' => 'Articles publiés',
			'pending'     => 'Articles en attente de validation',
			'rejected'   => 'Articles rejetés',
		],
		'charts' => [
			'items'      => "Vue d'ensemble des articles publiés",
			'categories' => "Vue d'ensemble des catégories publiés",
		]
	]
];
