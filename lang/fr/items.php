<?php

/*
|--------------------------------------------------------------------------
| Items Language Lines
|--------------------------------------------------------------------------
*/

return [
	'show'       => [
		'read'   => 'lecture',
		'hour'   => 'heure|heures',
		'min'    => 'min',
		'minute' => 'minute|minutes',
		'sec'    => 'sec',
		'second' => 'seconde|secondes',
	],
];
