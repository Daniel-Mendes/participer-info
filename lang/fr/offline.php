<?php

/*
|--------------------------------------------------------------------------
| Offline Language Lines
|--------------------------------------------------------------------------
*/

return [
	'title' => 'Il semble que vous ayez perdu votre connexion.',
	'text'  => 'Veuillez vérifier et réessayer.'
];
