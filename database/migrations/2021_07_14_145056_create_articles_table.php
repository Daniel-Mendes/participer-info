<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('articles', function (Blueprint $table) {
			$table->id();
			$table->string('url')->unique();
			$table->string('title');
			$table->string('slug')->unique();
			$table->text('description');
			$table->string('image');
			$table->tinyInteger('status')->default(0);
			$table->unsignedBigInteger('category_id')->nullable()->default(null);
			$table->unsignedBigInteger('feed_id')->nullable()->default(null);
			$table->timestamps();

			$table->foreign('category_id')->references('id')->on('categories');
			$table->foreign('feed_id')->references('id')->on('feeds');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('articles');
	}
};
