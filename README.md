# Participer à l'information

**participer.info** est une plateforme qui a pour but de partager des articles, vidéos, news, etc., afin de mettre en avant les visions alternatives des citoyens.

## Installation

```bash
git clone https://gitlab.com/Daniel-Mendes/participer-info.git
```

## Utilisation

```bash
php artisan serve
```

## Contribution
Les merge requests sont les bienvenues. Pour les changements majeurs, veuillez d'abord ouvrir un ticket pour discuter de ce que vous souhaitez changer.

Veillez à mettre à jour les tests si nécessaire.

## Licence
[GNU GPLv3](https://www.gnu.org/licenses/gpl-3.0.fr.html)
