<?php

namespace App\Console\Schedules;

use App\Enums\ArticleStatus;
use App\Models\Article;
use App\Models\Category;
use App\Models\Feed;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\Eloquent\InvalidCastException;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use InvalidArgumentException;
use Feeds;
use OpenGraph;
use SimplePie_Item;

class CheckNewArticlesInFeed
{
	/**
	 * Check if new items are available in the feed
	 *
	 * @return void
	 */
	public function __invoke()
	{
		$feeds = Feed::pluck('url', 'id')->toArray();
		$categories = Category::pluck('slug', 'id')->toArray();

		$simplePie = Feeds::make($feeds);

		/** @var SimplePie_Item $item */
		foreach ($simplePie->get_items() as $item) {
			if (!Article::where('url', $item->get_permalink())
				->orWhere('slug', Str::slug(strval($item->get_title())))->exists()) {
				$openGraph = OpenGraph::fetch($item->get_link());

				$imagePath = $this->storeImage($openGraph);

				$categoryId = $this->getCategoryOfItem($item, $categories);
				$feedId = $this->getFeedOfItem($item, $feeds);

				$this->createArticle($openGraph, $categoryId, $feedId, $imagePath);

				$this->updateFeedLastSync($feedId);
			}
		}
	}

	/**
	 * Store the article
	 */
	private function createArticle(array $openGraph, ?int $categoryId, ?int $feedId, string $imagePath) : void
	{
		$article = new Article();

		$article->url = $openGraph['url'];
		$article->slug = Str::slug($openGraph['title'], '-');
		$article->title = $openGraph['title'];
		$article->description = $openGraph['description'];
		$article->author = $openGraph['site_name'];
		$article->image = $imagePath;
		$article->status = ArticleStatus::PENDING->value;
		$article->feed_id = $feedId;
		$article->category_id = $categoryId;

		$article->save();
	}

	/**
	 * Store the image in the storage
	 */
	private function storeImage(array $openGraph) : string
	{
		$imageUrl = '';

		if (isset($openGraph['image:secure_url'])) {
			$imageUrl = $openGraph['image:secure_url'];
		} elseif (isset($openGraph['image'])) {
			$imageUrl = $openGraph['image'];
		}

		$filename = '';
		if ($imageUrl != '') {
			$parseUrl = parse_url($imageUrl, PHP_URL_PATH);
			if (!empty($parseUrl)) {
				$filename = Str::slug($openGraph['title'], '-') . '.' . pathinfo($parseUrl, PATHINFO_EXTENSION);
			}
			$contents = file_get_contents($imageUrl);
			if ($contents !== false) {
				Storage::disk('public')->put('articles/' . $filename, $contents);
			}
		}

		return "/storage/articles/$filename";
	}

	/**
	 * Get the category of the item
	 *
	 * @return int|null
	 */
	private function getCategoryOfItem(SimplePie_Item $item, array $categories)
	{
		$categoryId = null;

		if ($item->get_categories()) {
			foreach ($item->get_categories() as $category) {
				if (in_array(Str::slug(strval($category->get_label())), $categories)) {
					$categoryId = (int) array_search(Str::slug(strval($category->get_label())), $categories);
				}
			}
		}

		return $categoryId;
	}

	/**
	 * Get the feed of the item
	 *
	 * @return int|null
	 */
	private function getFeedOfItem(SimplePie_Item $item, array $feeds)
	{
		$feedId = null;

		if (in_array($item->get_feed()->feed_url, $feeds)) {
			$feedId = (int) array_search($item->get_feed()->feed_url, $feeds);
		}

		return $feedId;
	}

	/**
	 * Update the last sync of the feed
	 *
	 * @throws ModelNotFoundException
	 * @throws InvalidArgumentException
	 * @throws InvalidCastException
	 */
	private function updateFeedLastSync(?int $feedId) : void
	{
		$feed = Feed::findOrFail($feedId);
		$feed->last_sync = Carbon::now();
		$feed->save();
	}
}
