<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;

class UserController extends Controller
{
	/**
	 * Display a listing of the resource.
	 */
	public function index() : Response
	{
		$users = User::orderByDesc('created_at')->paginate(10);

		return response()->view('admin.users.index', compact('users'));
	}

	/**
	 * Display the specified resource.
	 */
	public function show(int $id) : Response
	{
		$user = User::findOrFail($id);

		return response()->view('admin.users.show', compact('user'));
	}

	/**
	 * Show the form for editing the specified resource.
	 */
	public function edit(int $id) : Response
	{
		$user = User::findOrFail($id);

		return response()->view('admin.users.edit', compact('user'));
	}

	/**
	 * Remove the specified resource from storage.
	 */
	public function destroy(int $id) : RedirectResponse
	{
		$user = User::findOrFail($id);

		$user->delete();

		return response()->redirectToRoute('admin.users.index');
	}
}
