<?php

namespace App\Http\Controllers\Admin;

use App\Enums\ArticleStatus;
use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateArticleRequest;
use App\Models\Article;
use App\Models\Category;
use App\Models\Feed;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;

class ArticleController extends Controller
{
	/**
	 * Display a listing of the resource.
	 */
	public function index() : Response
	{
		$articles = Article::orderByDesc('created_at')->paginate(10);

		return response()->view('admin.articles.index', compact('articles'));
	}

	/**
	 * Display a listing of the resource by status.
	 */
	public function status(Request $request) : Response
	{
		$articles = Article::orderByDesc('created_at')
			->where('status', $request->status)
			->with('category', 'feed')
			->paginate(10);

		$status = $request->status;

		return response()->view(
			'admin.articles.status',
			compact(['status', 'articles'])
		);
	}

	/**
	 * Display the specified resource.
	 */
	public function show(int $id) : Response
	{
		$article = Article::where('id', $id)->with('category')->first();

		return response()->view('admin.articles.show', compact('article'));
	}

	/**
	 * Show the form for editing the specified resource.
	 */
	public function edit(int $id) : Response
	{
		$article = Article::findOrFail($id);

		$categories = Category::all();

		$feeds = Feed::all();

		return response()->view(
			'admin.articles.edit',
			compact(['article', 'categories', 'feeds'])
		);
	}

	/**
	 * Update the specified resource in storage.
	 */
	public function update(UpdateArticleRequest $request, int $id) : RedirectResponse
	{
		$article = Article::findOrFail($id);

		$validated = $request->validated();

		$article->url = strval($validated['url']);
		$article->title = strval($validated['title']) ;
		$article->slug = strval($validated['slug']);
		$article->description = strval($validated['description']);
		$article->author = strval($validated['author']);

		if ($validated["category"] != null) {
			$article->category_id = intval($validated['category']);
		}

		if ($request->hasFile('image')) {
			$article->image = $validated->photo->storeAs(
				'public/articles',
				$validated['slug'] . '.' . $validated->photo->extension()
			);
		}

		if ($article->status->value != $validated['status']) {
			$article->status = intval($validated['status']);
			$article->published_at = null;

			if ($validated['status'] == ArticleStatus::PUBLISHED->value) {
				$article->published_at = Carbon::now();
			}
		}

		$article->save();

		return response()->redirectToRoute(
			'admin.articles.show',
			$article->id
		)->with('success', "L'article a été mis à jour avec succès.");
	}

	/**
	 * Remove the specified resource from storage.
	 */
	public function destroy(int $id) : RedirectResponse
	{
		$article = Article::findOrFail($id);

		$article->delete();

		return response()->redirectToRoute('admin.articles.index');
	}
}
