<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Article;
use App\Models\Feed;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class FeedController extends Controller
{
	/**
	 * Display a listing of the resource.
	 */
	public function index() : Response
	{
		$feeds = Feed::orderByDesc('id')->paginate(10);

		return response()->view('admin.feeds.index', compact('feeds'));
	}

	/**
	 * Show the form for creating a new resource.
	 */
	public function create() : Response
	{
		return response()->view('admin.feeds.create');
	}

	/**
	 * Store a newly created resource in storage.
	 */
	public function store(Request $request) : RedirectResponse
	{
		$request->validate([
			'name' => 'required||max:255',
			'slug' => 'required|unique:feeds|max:255',
			'url' => 'required|unique:feeds|max:255|url',
		]);

		$feed = Feed::create($request->all());

		return response()->redirectToRoute('admin.feeds.show', $feed->id)
			->with('success', 'Le flux RSS a été créé avec succès.');
	}

	/**
	 * Display the specified resource.
	 */
	public function show(int $id) : Response
	{
		$feed = Feed::findOrFail($id);

		$articles = $feed->articles()->paginate(10);

		return response()->view(
			'admin.feeds.show',
			compact('feed', 'articles')
		);
	}

	/**
	 * Show the form for editing the specified resource.
	 */
	public function edit(int $id) : Response
	{
		$feed = Feed::findOrFail($id);

		return response()->view('admin.feeds.edit', compact('feed'));
	}

	/**
	 * Update the specified resource in storage.
	 */
	public function update(Request $request, int $id) : RedirectResponse
	{
		$request->validate([
			'name' => 'required||max:255',
			'slug' => 'required|unique:feeds,id,' . $id . '|max:255',
			'url' => 'required|unique:feeds,id,' . $id . '|max:255|url',
		]);

		Feed::findOrFail($id)->update($request->all());

		return response()->redirectToRoute('admin.feeds.show', $id)
			->with('success', 'Le flux RSS a été mis à jour avec succès.');
	}

	/**
	 * Remove the specified resource from storage.
	 */
	public function destroy(int $id) : RedirectResponse
	{
		Article::where('feed_id', $id)->update(['feed_id' => null]);
		Feed::findOrFail($id)->delete();

		return response()->redirectToRoute('admin.feeds.index')
			->with('success', 'Le flux RSS a été supprimé avec succès.');
	}
}
