<?php

namespace App\Http\Controllers\Admin;

use App\Enums\ArticleStatus;
use App\Http\Controllers\Controller;
use App\Models\Article;
use Carbon\Carbon;
use Illuminate\Http\Response;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use InvalidArgumentException;

class AdminController extends Controller
{
	/**
	 * Show the application dashboard.
	 */
	public function index() : Response
	{
		$articlesCountGroupByStatus = $this->articlesCountGroupByStatus();
		$articlesChartjsData = $this->articlesChartjsData();
		$categoriesChartjsData = $this->categoriesChartjsData();

		return response()->view(
			'admin.index',
			compact([
				'articlesCountGroupByStatus',
				'articlesChartjsData',
				'categoriesChartjsData',
			])
		);
	}

	/**
	 * Get the number of articles grouped by status.
	 *
	 * @return Collection<int, \App\Models\Article>
	 */
	private function articlesCountGroupByStatus() : Collection
	{
		$articlesCountGroupByStatus =
			Article::groupBy('status')
			->select(DB::raw('count(*) as total'))
			->orderBy('status')
			->get();

		return $articlesCountGroupByStatus;
	}

	/**
	 * Get the chartjs data for articles.
	 */
	private function articlesChartjsData() : array
	{
		$articles =
			Article::groupBy('year-month', 'month')
			->selectRaw("DATE_FORMAT(published_at, '%Y-%m') as 'year-month',
							DATE_FORMAT(published_at, '%b') as month,
							count(*) as count")
			->where('published_at', '>=', Carbon::now()->subMonths(12))
			->where('status', ArticleStatus::PUBLISHED)
			->orderBy('year-month')
			->get();

		$articlesChartjsData = [
			'labels' => array_column($articles->toArray(), 'month'),
			'data' => [],
		];

		foreach ($articles as $article) {
			array_push(
				$articlesChartjsData['data'],
				$article->count
			);
		}

		return $articlesChartjsData;
	}

	/**
	 * Get the chartjs data for categories.
	 */
	private function categoriesChartjsData() : array
	{
		$categoriesChartjsData = [
			'labels' => [],
			'data' => [],
		];

		$categories = Article::groupBy('category_id')
			->where('status', ArticleStatus::PUBLISHED)
			->with('category')
			->selectRaw('count(*) as count, category_id')
			->get();

		foreach ($categories as $category) {
			if (!empty($category->category_id) && $category->category) {
				array_push(
					$categoriesChartjsData['labels'],
					$category->category->name
				);
				array_push(
					$categoriesChartjsData['data'],
					$category->count
				);
				continue;
			}

			array_push(
				$categoriesChartjsData['labels'],
				'Aucune'
			);
			array_push(
				$categoriesChartjsData['data'],
				$category->count
			);
		}

		return $categoriesChartjsData;
	}
}
