<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CategoryController extends Controller
{
	/**
	 * Display the specified resource.
	 */
	public function show(Request $request) : Response
	{
		$categories = Category::orderBy('slug')->get();

		$categoryActive = Category::where('slug', $request->category)->firstOrFail();

		return response()->view('categories.show', compact(['categories', 'categoryActive']));
	}
}
