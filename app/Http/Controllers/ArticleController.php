<?php

namespace App\Http\Controllers;

use App\Enums\ArticleStatus;
use App\Http\Requests\StoreArticleRequest;
use App\Mail\ArticleCreated;
use App\Models\Article;
use App\Models\Category;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use OpenGraph;

class ArticleController extends Controller
{
	/**
	 * Display a listing of the resource.
	 */
	public function index() : Response
	{
		$categories = Category::orderBy('slug')->get();

		return response()->view('articles.index', compact('categories'));
	}

	/**
	 * Show the form for creating a new resource.
	 */
	public function create() : Response
	{
		$categories = Category::orderBy('slug')->get();

		return response()->view('articles.create', compact('categories'));
	}

	/**
	 * Store a newly created resource in storage.
	 */
	public function store(StoreArticleRequest $request) : JsonResponse
	{
		$validated = $request->validated();

		$article = new Article;

		$article->url = strval($validated['url']);
		$article->title = strval($validated['title']);
		$article->slug = Str::slug(strval($validated['title']), '-');
		$article->description = strval($validated['description']);
		$article->author = strval($validated['author']);
		$article->image = $this->storeImageToStorage(strval($validated['image']), strval($article->slug));
		$article->status = ArticleStatus::PENDING->value;
		$article->category_id = intval($validated['category']);

		$article->save();

		Mail::to(env('MAIL_TO_ADDRESS'))->send(new ArticleCreated($article));

		return response()->json(['success' => true], 201);
	}

	/**
	 * Store image to storage
	 */
	private function storeImageToStorage(string $image, string $filename) : string
	{
		$parsedImageUrl = parse_url($image, PHP_URL_PATH);
		$extension = pathinfo(strval($parsedImageUrl), PATHINFO_EXTENSION);

		$filename = $filename . '.' . $extension;
		$contents = file_get_contents($image);

		if ($contents !== false) {
			Storage::disk('public')->put('articles/' . $filename, $contents);
		}

		return "/storage/articles/$filename";
	}

	/**
	 * Fetch OpenGraph data for an article.
	 */
	public function fetch(Request $request) : JsonResponse
	{
		if (Article::firstWhere('url', $request->url)) {
			return response()->json(['status' => 'error', 'message' => "L'URL existe déjà."], 409);
		}

		return response()->json([OpenGraph::fetch($request->url)], 200);
	}
}
