<?php

namespace App\Http\Controllers\Api;

use App\Enums\ArticleStatus;
use App\Http\Controllers\Controller;
use App\Models\Article;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
	/**
	 * Display the specified resource.
	 */
	public function show(Request $request) : JsonResponse
	{
		$articles = Article::whereHas('category', function ($query) use ($request) {
			$query->where('status', ArticleStatus::PUBLISHED);
			$query->where('slug', $request->category);
		})->orderByDesc('published_at')->cursorPaginate(10);

		return response()->json($articles);
	}
}
