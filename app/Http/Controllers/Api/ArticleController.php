<?php

namespace App\Http\Controllers\Api;

use App\Enums\ArticleStatus;
use App\Http\Controllers\Controller;
use App\Models\Article;
use Illuminate\Http\JsonResponse;

class ArticleController extends Controller
{

	/**
	 * Display the specified resource.
	 */
	public function index() : JsonResponse
	{
		$articles = Article::where('status', ArticleStatus::PUBLISHED)
			->orderByDesc('published_at')
			->with('category')
			->cursorPaginate(10);

		return response()->json($articles);
	}
}
