<?php

namespace App\Http\Requests;

use App\Enums\ArticleStatus;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Enum;

class UpdateArticleRequest extends FormRequest
{
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'url' => 'required|url|unique:articles,url,' . $this->route('article'),
			'title' => 'required|string|max:255',
			'slug' => 'required|string|max:255|unique:articles,slug,' . $this->route('article'),
			'description' => 'required|string|max:255',
			'author' => 'required|string|max:255',
			'image' => 'nullable|url',
			'status' => [
				'required',
				new Enum(ArticleStatus::class),
			],
			'category' => 'nullable|integer|exists:categories,id',
		];
	}
}
