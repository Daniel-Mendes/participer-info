<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreArticleRequest extends FormRequest
{
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'url' => 'required|unique:articles|url',
			'title' => 'required|string|max:255',
			'description' => 'required|string|max:255',
			'author' => 'required|string|max:255',
			'image' => 'required|url',
			'category' => 'required|integer|exists:categories,id',
		];
	}
}
