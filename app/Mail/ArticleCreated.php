<?php

namespace App\Mail;

use App\Models\Article;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ArticleCreated extends Mailable implements ShouldQueue
{
	use Queueable, SerializesModels;

	/**
	 * The article instance.
	 */
	private Article $article;

	/**
	 * Create a new message instance.
	 *
	 * @return void
	 */
	public function __construct(Article $article)
	{
		$this->article = $article;
	}

	/**
	 * Build the message.
	 *
	 * @return $this
	 */
	public function build()
	{
		return $this->markdown('emails.articles.created', [
			'article' => $this->article,
		]);
	}
}
