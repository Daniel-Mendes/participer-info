<?php

namespace App\Models;

use App\Enums\ArticleStatus;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Laravel\Scout\Searchable;
use Spatie\Feed\Feedable;
use Spatie\Feed\FeedItem;

/**
 * App\Models\Article
 *
 * @property int $id
 * @property string $url
 * @property string $title
 * @property string $slug
 * @property string $description
 * @property string $author
 * @property string $image
 * @property int $status
 * @property int|null $category_id
 * @property int|null $feed_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Category|null $category
 * @property-read \App\Models\Feed|null $feed
 * @method static \Illuminate\Database\Eloquent\Builder|Article newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Article newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Article query()
 * @method static \Illuminate\Database\Eloquent\Builder|Article whereAuthor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Article whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Article whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Article whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Article whereFeedId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Article whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Article whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Article whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Article whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Article whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Article whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Article whereUrl($value)
 * @mixin \Eloquent
 * @property \Illuminate\Support\Carbon|null $published_at
 * @method static \Illuminate\Database\Eloquent\Builder|Article wherePublishedAt($value)
 */
class Article extends Model implements Feedable
{
	use HasFactory, Searchable;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array<string>
	 */
	protected $fillable = [
		'url',
		'title',
		'slug',
		'description',
		'author',
		'image',
		'status',
		'published_at',
		'category_id',
		'feed_id',
	];

	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'articles';

	/**
	 * The primary key associated with the table.
	 *
	 * @var string
	 */
	protected $primaryKey = 'id';

	/**
	 * Get the category of the article.
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo<\App\Models\Category, \App\Models\Article>
	 */
	public function category() : BelongsTo
	{
		return $this->belongsTo(Category::class, 'category_id');
	}

	/**
	 * Get the feed of the article.
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo<\App\Models\Feed, \App\Models\Article>
	 */
	public function feed() : BelongsTo
	{
		return $this->belongsTo(Feed::class, 'feed_id');
	}

	/**
	 * The attributes that should be cast to native types.
	 *
	 * @var array<string, string>
	 */
	protected $casts = [
		'email_verified_at' => 'datetime',
		'published_at' => 'datetime',
		'status' => ArticleStatus::class,
	];

	/**
	 * Algolia add attributes
	 */
	public function toSearchableArray() : array
	{
		$array = $this->toArray();

		$array = $this->transform($array);

		$array['category_name'] = "";
		$array['feed_name'] = "";

		if (!is_null($array['category_id']) && !is_null($this->category)) {
			$array['category_name'] = $this->category->name;
		}

		if (!is_null($array['feed_id']) && !is_null($this->feed)) {
			$array['feed_name'] = $this->feed->name;
		}

		return $array;
	}

	/**
	 * Return the feed item for the article.
	 */
	public function toFeedItem(): FeedItem
	{
		return FeedItem::create()
			->id((string) $this->id)
			->title($this->title)
			->summary($this->description)
			->category($this->category ? $this->category->name : "")
			->updated($this->published_at)
			->link($this->url)
			->authorName($this->author)
			->image($this->image);
	}

	/**
	 * Return articles for feed.
	 *
	 * @return \Illuminate\Database\Eloquent\Collection<int, \App\Models\Article>
	 */
	public static function getFeedItems() : Collection
	{
		return Article::where('status', ArticleStatus::PUBLISHED)
			->orderByDesc('published_at')
			->limit(50)
			->with('category')
			->get();
	}
}
